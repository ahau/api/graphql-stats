# GraphQL for machine statistics

Machine statistics types and resolvers.
It provides primitives for reading cpu load, memory load, disk usage and network information.

## Usage

`npm i -S @ssb-graphql/stats`

## Example Usage

```js
const { ApolloServer } = require('apollo-server-express')
const { buildFederatedSchema } = require('@apollo/federation');

const stats = require('@ssb-graphql/stats')

const server = new ApolloServer({
  schema: buildFederatedSchema([
    { typeDefs: stats.typeDefs, resolvers: stats.resolvers },
    // add other types + resolvers here!
  ])
})
```

## API

See `/src/typeDefs.js` for the most up to date details on what's offered by this module.

## Testing

run `npm test` to run tests

