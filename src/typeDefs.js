const { gql } = require('graphql-tag')

module.exports = gql`
  type DiskInfo {
    use: Float
    size: Float
    used: Float
    fs: String
    type: String
    mount: String
  }
  type NetworkInfo {
    defaultGateway: String
    defaultInterface: String
    publicIpv4: String
    ipv4: String
    ip4subnet: String
    publicIpv6: String
    ipv6: String
    ip6subnet: String
    type: String
    macAddress: String
    internetLatency: Float
    portForwarding: Boolean
  }
  extend type Query {
    cpuLoad: Float
    memoryLoad: Float
    diskUsage: [DiskInfo]
    network: NetworkInfo
  }
`
