import AhauClient from 'ahau-graphql-client'

const Server = require('scuttle-testbot')

const ahauServer = require('ahau-graphql-server')
const fetch = require('node-fetch')

module.exports = async function (opts = {}) {
  opts = {
    port: 3000 + (Math.random() * 1e4) | 0,
    ...opts
  }
  const ssb = Server(opts)

  const port = 3000 + Math.random() * 7000 | 0
  const httpServer = await ahauServer({
    schemas: [
      require('..')(ssb)
    ],
    context: {},
    port
  })
  ssb.close.hook((close, [cb]) => {
    httpServer.close()
    close(cb)
  })

  const apollo = new AhauClient(port, { isTesting: true, fetch })

  return {
    ssb,
    apollo
  }
}
